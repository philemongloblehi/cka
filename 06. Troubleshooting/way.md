Troubleshooting
---

### Is pod running ?

`kubectl get pod POD_NAME`

### Is pod registered with service ?

`kubectl get ep`

`kubectl describe service SERVICE_NAME`

### Is service accessible ?

`nc SERVICE_IP SERVICE_PORT`

`ping SERVICE_NAME` (from a pod)

### Check app logs 

`kubectl logs POD_NAME`

### Check pod status and recent events

`kubectl describe pod POD_NAME`
