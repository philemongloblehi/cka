Busybox
---

## Accéder aux conteneur Busybox

### Option 1 :

`kubectl run debug --image=busybox -it`

### Option 2 :

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: debug
  name: debug
spec:
  containers:
  - image: busybox
    name: debug
    command: ["sh","-c","sleep 1000"]
``` 

| Docker | Kubernetes |
| ------ | ------ |
| ENTRYPOINT | command |
| CMD | args |

EXEMPLE :  Ecraser CMD ou ENTRYPOINT du Dockerfile depuis le manifest Kubernetes

```
# Dockerfile
FROM Ubuntu
ENTRYPOINT ["printenv"]
CMD ["HOSTNAME","KUBERNETES_PORT"]
```
```yaml
#yaml file 
...
spec:
  containers:
  - image: xxx
    name: xxx
    command: ["printenv"]
    args: ["HOSTNAME","KUBERNETES_PORT"]
```



