### Qu'est ce que Kubernetes ?

Kubernetes est un Outil Open source

Il permet de manager les conteneurs sur différents environnements (Cloud/Local)

### Quel est le problème que Kubernetes résoud ?

La découpe d'une application monolithique pour adopter une architecture microservice peut engendrer une dizaine voire centaine de microservice

L'administration de ces microservices de facon manuelle ou par des scripts peut s'avérer très délicat voire impossible 

Kuberenetes répond à ce besoin d'administrartion

### Quelles sont les fonctionnalités de Kubernnetes ?

- Haute Disponibilité (Pas de temps d'interruption)
- Scalabilité (Scale up / Scale down)
- Disaster recovery (Backup / Restore)
