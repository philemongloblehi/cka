ConfigMap & Secret
---

### As Env

```yaml
apiVersion: v1
kind: configMap
metadata:
  name: test-configmap
data:
  db_host: mysql-service
```

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: test-secret
type: Opaque
data:
  username: bXl1c2Vy
  password: bXlwYXNz
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp
  labels:
    app: app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: app
  template:
    metadata:
      labels:
        app: app
    spec:
      containers:
      - name: myapp
        image: busybox
        command: ["sh","-c"]
        args:
        - printenv MYSQL_USER MYSQL_PWD MYSQL_SERVER;
        - sleep 100;
        env:
        - name: MYSQL_USER
          valueFrom:
            secretKeyRef:
              name: test-secret
              key: username
        - name: MYSQL_PWD
          valueFrom:
            secretKeyRef:
              name: test-secret
              key: password
        - name: MYSQL_SERVER
          valueFrom:
            configMapKeyRef:
              name: test-configmap
              key: db_host
```
