ConfigMap & Secret
---

### As volume

```yaml
apiVersion: v1
kind: configMap
metadata:
  name: mysql-config-file
data:
  mysql.conf: |
    [mysqld]
    port=3306
    socket=/tmp/mysql.sock
    key_buffer_size=16M
    max_allowed_packet=128M
```

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mysql-secret-file
type: Opaque
data:
  secret.file: |
    YSBjaGFuZ2UK
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp
  labels:
    app: app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: app
  template:
    metadata:
      labels:
        app: app
    spec:
      containers:
      - name: myapp
        image: mysql
        command: ["sh","-c","cat /mysql/db-configmap/mysql.conf; cat /mysql/db-secret/secret.file; sleep 20"]

        volumeMounts:
          - name: mysql-configmap-volume
            mountPath: /mysql/db-configmap

          - name: mysql-secret-volume
            mountPath: /mysql/db-secret
            readOnly: true

      volumes:
        - name: mysql-configmap-volume
          configMap:
            name:  mysql-config-file

        - name: mysql-secret-volume
          secret:
            secretName: mysql-secret-file
```

If a change happen on configmap or secret, the pod will not be updated automatically.
We have to rollout the deployment 

`kubectl rollout restart deployment/myapp`
