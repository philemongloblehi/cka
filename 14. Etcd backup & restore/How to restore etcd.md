How to restore etcd 
---

1.  Execute the restore

`sudo ETCDCTL_API=3 etcdctl --data-dir /var/lib/etcd-backup snapshot restore /tmp/etcd-snapshot.db`

2. Let etcd knows about the change 

`/etc/kubernetes/manifests/etcd.yaml`

```
...
  - hostPath:
      path: /var/lib/etcd-backup  <--
      type: DirectoryOrCreate
    name: etcd-data
status: {}
```

💡 Kubelet will apply the change automatically
