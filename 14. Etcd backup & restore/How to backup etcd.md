How to backup etcd 
---

1. Install etcdctl 

`sudo apt install etcd-client`


`etcdctl --version`

2. Execute the backup

`sudo ETCDCTL_API=3 etcdctl snapshot save /tmp/etcd-snapshot.db --endpoints=https://127.0.0.1:2379 --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/apiserver-etcd-client.crt --key=/etc/kubernetes/pki/apiserver-etcd-client.key`

`--cacert`, `--cert`, `--key` and `--endpoints` can be found at the etcd (`/etc/kubernetes/manifests/etcd.yaml`) or apiserver (`/etc/kubernetes/manifests/kube-apiserver.yaml`) pod description 

3. Verify the backup

`ETCDCTL_API=3 etcdctl --write-out=table snapshot status /tmp/etcd-snapshot.db`
