Assigning pod to node
---

### Using NodeName

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-nodename
spec:
  containers:
  - name: nginx
    image: nginx
  nodeName: worker1
```

### Using NodeSelector

`kubectl label node worker2 type=cpu`

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-selector
spec:
  containers:
  - name: nginx
    image: nginx
  nodeSelector:
    type: cpu
```

### Using NodeAffinity

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-affinity
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: kubernetes.io/os
            operator: In
            values:
            - linux
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 1
        preference:
          matchExpressions:
          - key: type
            operator: In
            values:
            - cpu
  containers:
  - name: with-node-affinity
    image: nginx
```

Operator : `In`, `NotIn`, `Exists`, `DoesNotExist`, `Gt`, `Lt`, `NotIn` and `DoesNotExist`
