Inter-Pod affinity
---

`Inter-Pod affinity` = Schedule our pods apps only on node that have a specific pod running on them

`Inter-Pod anti-affinity` = Schedule our pods app only on node that don't already have a my-app replica running on them

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp-pod-affinity
  labels:
    app: myapp
spec:
  replicas: 5
  selector:
    matchLabels:
      app: myapp
  template:
    metadata:
      labels:
        app: myapp
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
      tolerations:
      - operator: Exists
        effect: NoSchedule
      nodeSelector: 
        type: master
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: component
                operator: In
                values:
                - etcd
            topologyKey: kubernetes.io/hostname
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 1
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - myapp
              topologyKey: kubernetes.io/hostname
```
