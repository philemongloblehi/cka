Taints and Tolerations
---

Déployer un pod sur le master qui a une taint

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-with-toleration
  labels:
    env: test
spec:
  containers:
  - name: nginx
    image: nginx
  tolerations:
  - operator: Exists
    effect: NoSchedule
  nodeName: master
```
