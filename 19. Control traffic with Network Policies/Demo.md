```pdf
Créer 3 déploiements : frontend, backend et database
- frontend ne doit pouvoir communiquer qu'avec backend
- database autorise les entrées de backend uniquement ET n'autorise aucune sortie 
```

```yaml 
# Frontend 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend-deployment
  namespace: myapp
  labels:
    app: frontend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
      - name: node
        image: node:16-alpine
        ports:
        - containerPort: 3000
        command: ["sh", "-c", "sleep 3000"]
```

```yaml
# Backend
apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend-deployment
  namespace: myapp
  labels:
    app: backend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - name: nginx
        image: nginx:1.21-alpine
        ports:
        - containerPort: 80
```

```yaml
# Database
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis-deployment
  namespace: myapp
  labels:
    app: redis
spec:
  replicas: 2
  selector:
    matchLabels:
      app: redis
  template:
    metadata:
      labels:
        app: redis
    spec:
      containers:
      - name: redis
        image: redis:6-alpine
        ports:
        - containerPort: 6379
```

```yaml
# Network Policy Frontend
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: np-frontend
  namespace: myapp
spec:
  podSelector:
    matchLabels:
      app: frontend
  policyTypes:
    - Egress
  egress:
    - to:
      - podSelector:
          matchLabels:
            app: backend
      ports:
        - protocol: TCP
          port: 80
```

```yaml
# Network Policy Backend
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: np-database
  namespace: myapp
spec:
  podSelector:
    matchLabels:
      app: redis
  policyTypes:
    - Ingress
    - Egress # Lorsque pas de paramètres précisés, bloque toutes les sorties 
  ingress:
    - from:
      - podSelector:
          matchLabels:
            app: backend
      ports:
        - protocol: TCP
          port: 6379
```

VERIFY THE CONNEXION BETWEEN PODS

`kubectl exec redis-deployment-86dc64f885-rnc7v -- sh -c "nc -v 10.36.0.3 80"`
