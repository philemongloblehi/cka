### Role Based Access Control 

---
```
# Role : Permet de définir les permissions sur les composants du namespace (pod, service, etc) 
Les roles ne sont délimités qu'aux Namespaces
```
```
# RoleBinding : Permet de lier un Role à un user/group
```
```
# ClusterRole : Permet de définir les permissions sur le cluster (Node etc)
```
```
# ClusterRoleBinding : Permet de lier un clusterRole à un admin/groupAdmin
```
```
# Service Account : Composant kubernetes représentant une application/service
```
