### Create Service Account and give permissions to the cluster 

---

1. Create Service Account 

`kubectl create serviceaccount jenkins --dry-run=client -o yaml > jenkins-sa.yaml`

`kubectl apply -f jenkins-sa.yaml`

2. Get the serviceaccount token

`kubectl describe sa jenkins`

`kubectl describe secret <secret>`

The base64 decode token is in the output

💡 For simplicity `token=<copied_token>`

---

### Connect to the server using Service Account 

1. Using Command Line 

💡 `kubectl options` for all options of the command 

`kubectl --server=https://172.31.18.169:6443 --certificate-authority /etc/kubernetes/pki/ca.crt --token $token get pod`

2. Kubeconfig file 

`💡 ~/.kube/config`

Exemple : 
```
# jenkins.conf

apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM1ekNDQWMrZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJeU1EWXdOekUyTWpFeU1Wb1hEVE15TURZd05ERTJNakV5TVZvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTTMvCm45V3d5Q25qakwwRmdzbXp3Z1A1cUc2VTI3M2FKbkdBdWdaSWN3eHQ3b0p4aGl6bndjcGxMdnpFY2Nwa3dDODIKQURvWTJSQm4rckFvZVhxeGM1ZEJWNHc4ZDlVa09oRVlxYldnTlhQUUZJc2QxRTRQTkg3MlVieFRPeURwVnAwOQpjOGFrSUJjVnZnWHRxakNOKzRibDhkU3NoRDFHKzlFYWw2K2k3UUJVQlMzWTVUSGVmMEg0WmlabmpzREVOdVNwCmxVVVFzdDhEbmNFZVNpaGRHZVcwYUhXeVB0cXF1NysybTFNR3Myb2xhWTRyYTIzcDFyQjl0ZEYwL1hlUWh0SmQKN0F2enVOWTBQZDZvVGtmc0ovNklPNU9BWU9KeGpuVXVvd3VRNFJadEUwMVVsMGFrRjd6TUVESTl5TDRHMHQvcworZnI0Z2p4WTM4TC9OZWI5YjNNQ0F3RUFBYU5DTUVBd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0hRWURWUjBPQkJZRUZBWmtBc2JhTnFMa0ljVVQwT2lXeFM2VXAvbVhNQTBHQ1NxR1NJYjMKRFFFQkN3VUFBNElCQVFCUzR6SUhOTzI5WDBCbkFHL2NiNkJoSUs4MCtBQ3dnN0ZHR25SakZUaXV2WnFWQ1RmaApLaVptME1NL25Zd3d4dmt4NWJJMUFnRm1SWE9GMzhFTlllMW1FYS9GRjQ3WklTVjByV01ONnNmbk1KUmdPbCs1CmlSQ0wyNlFmclJMQTVZemFjK1dwQ2lYdXR5SUNaWUtOL3djd1g2VlA2aENiSXZVWEF0NUF3ckZWMHAvRmRyTWQKbkl4TnJENkZ5VEQ1dTFWZVc4cmlFT2duY2ZKUEduRjJWcS9OYlBZTnArK3laK1Ixd0hQalNxZzl2SWFodUxXVAo5Z2E2K09oSDlRWFdXTFVoWldINnRidVdxUENWZTloWjFCa2hNbFN5T2s4WE8zVHpETDlCQzB0VzRxLzJsVW11ClQydnZGNEFvZjBHaEIzdEFCTVZFQnNXcWx1YUdldGJNZjl0TwotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    server: https://172.31.18.169:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: jenkins
  name: jenkins@kubernetes
current-context: jenkins@kubernetes
kind: Config
preferences: {}
users:
- name: jenkins
  user:
   token: eyJhbGciOiJSUzI1NiIsImtpZCI6InBSZHBXUHhRUEpjMkJRV1VWdzdwTDBkMHZMcGdxTmxiU0tXTDE0bERhUXcifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImplbmtpbnMtdG9rZW4tbndzNmsiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiamVua2lucyIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6Ijg3ZGU2MmIxLWY4YWQtNGQ2MC1hNzYxLWUxZWNhNTU2MDc0YSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmplbmtpbnMifQ.W97e0AKRkBWU_miZlY_NDp8SoiqGgSAYNt9enVpoLIbgIr7eudZ2_F6wXD41Gtsd52UXRnSUmgzBX4w29f_Si8REYMddtY-Wz2o0aAa4DHH-6nfGTc4Qew7L0Ray5LeWcguwa2novdGJTurNYG8Rbp1UJtcmvG0PPAT6eGfwQYUAKTkMTyGEsE6E0Q3tUHptVQC6x_oAGXtiKAmCqPW7978V5slqrLS-FTrwc8UIP-ogHiTaS3NFb1NBepxPSc_XDcbKNUp-qqrXuRsouRYje39gfnQL43ebA20h0oIVfPUIMlFXy8xytcpSemkibaueQzt3aI2OB6VfLZKWrzRt0g
```
`kubectl --kubeconfig jenkins.conf get pod`

---
### Assign Permissions to the Service Account

1. Create Role

`kubectl create role jenkins-role --verb=create,update,list --resource=deployments.apps,services --dry-run=client -o yaml > jenkinsRole.yaml`

`kubectl apply -f jenkinsRole.yaml`

2. Create RoleBinding

`kubectl create rolebinding jenkinsRoleBinding --role jenkins-role --serviceaccount=default:jenkins --dry-run=client -o yaml > jenkinsRoleBinding.yaml`

`kubectl apply -f jenkinsRoleBinding.yaml`

3. Verify permission

`kubectl auth can-i create service --as system:serviceaccount:default:jenkins -n default`


