### Create a User Account signed by certificate 

1. Create Client Key (Private)

        `openssl genrsa -out dev-tom.key 2048` = Crée une clé rsa privé sous le nom dev-tom 

2. Generate Certificate Signing Request

        `openssl req -new -key dev-tom.key -subj "/CN=tom" -out dev-tom.csr` = Effectue une demande de signature de la clé précedemment créee

3. Send CSR to Kubernetes

    -  Créer un composant Kubernetes `CertificateSigningRequest` avec en request `dev-tom.csr` en base64 encoded
 
        Liens importants : https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/
        
        ```
        #file.yaml
        apiVersion: certificates.k8s.io/v1
        kind: CertificateSigningRequest
        metadata:
        name: dev-tom
        spec:
        request: `cat tom-dev.csr | base64 | tr -d "\n"`
        signerName: kubernetes.io/kube-apiserver-client
        # expirationSeconds: 8640000 (Commenter pour erreur.. Résolu à partir de Kubernetes 1.22 | Version utilisé 1.21)
        usages:
        - client auth
        ``` 

4. Kubernetes sign certificate for you (pending still approved by an admin)

        `kubectl apply -f file.yaml`

        `kubectl get csr`

5. Kubernetes admin approve the certificate 

        `kubectl certificate approve dev-tom`

6. Create Client Key (Public)

        Copier la partie `certificate` de `kubectl get csr -o yaml`

        Decode64 sous .crt 

        `echo "certificate" | base64 --decode > dev-tom.crt`

---

### Connect to the cluster using this user  

1. Using Command 

    `L'endpoint du serveur = kubectl cluster-info`

    ```
    kubectl --server=https://172.31.18.169:6443 \
    > --certificate-authority=/etc/kubernetes/pki/ca.crt \
    > --client-certificate=dev-tom.crt \
    > --client-key=dev-tom.key \
    > get pod
    ```

2. Using Kubeconfig

---

### Assign permission to this user

RBAC documentation : https://kubernetes.io/docs/reference/access-authn-authz/rbac/

API Reference : https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#-strong-api-overview-strong-

1. Create cluster role 

💡 On aurait pu créer un role mais dans notre cas on souhaite donné des permissions à travers tous les namespaces alors on crée un cluster role plutôt

`kubectl create clusterrole test-cr --verb=get,list,create,update,delete --resource=deployments.apps,pods --dry-run=client -o yaml > testClusterRole.yaml`

```
#testClusterRole.yaml (modifié)
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: test-cr
rules:
- apiGroups:
  - ""
  resources:
  - pods
  - services  
  verbs: ["*"]
- apiGroups:
  - apps
  resources:
  - deployments
  - statefulsets  
  verbs:
  - get
  - list
  - create
```

💡 Pour appliquer le fichier, SI pas admin du cluster :
`kubectl --kubeconfig config apply -f testClusterRole.yaml`

2. Create Cluster Role Binding 

Va permettre d'assigner les permissions à notre utilisateur tom 

`kubectl create clusterrolebinding test-crb --clusterrole=test-cr --user=tom --dry-run=client -o yaml > testClusterRoleBinding.yaml`

3. Vérifier les permissions 

`kubectl auth --kubeconfig config can-i create pod --as tom`

`kubectl auth --kubeconfig config can-i update deployment --as tom`





