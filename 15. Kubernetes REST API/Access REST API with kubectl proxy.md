Access REST API with kubectl proxy
---

`kubectl proxy --port=8080 &`

`curl http://127.0.0.1:8080`

`curl http://127.0.0.1:8080/api/v1/namespaces/`

`curl http://127.0.0.1:8080/api/v1/namespaces/default/`

`curl http://127.0.0.1:8080/api/v1/namespaces/default/services`

