# CKA

Certified Kubernetes Administrator

## Some interesting Command

`kubeadm init` = Initialise le cluster 

`kubectl cluster-info` = Affiche des infos sur le cluster

`kubectl api-resources` = Affiche des infos sur les différents composants kubernetes (très utile) 

`kubectl describe <component> <nom>` = Affiche des informations sur le composant 

`kubectl get ep` = Affiche les endpoints du cluster 

`kubectl get all` = Affiche tous les composants dans le namespace par défaut 

`kubectl get pods --all-namespaces` = Affiche tous les composants dans tous les namespaces 

`kubectl edit <component> <nom>` = Affiche le fichier de configuration du composant

`kubectl get <component> <nom> -o yaml` = Affiche les informations du composant au format yaml 

`kubectl get <component> <nom> --show-labels` = Affiche les infos avec une colonne labels 

`kubectl get <component> -l <label>` = Affiche les composants avec le labels précisés

`kubectl scale deployment <name> --replicas=04` = Scale up/down le nombre de replicas 

`kubectl scale deployment <name> --replicas=02 --record` = Enregistre le changement

`kubectl rollout history <component>/<name>` = Liste les enregistrements   

`kubectl run <name> --image=<image>` = Créer un pod 

`kubectl exec -it nginx-deployment-7848d4b86f-brtxk -- bash` = Ouvrir un terminal à l'intérieur du pod (si le pod n'a qu'un seul conteneur) 

`kubectl exec -it pod -- sh -c "echo hello"` = Exécuter une commande depuis le pod sans y accéder 

`kubectl create service clusterip test-svc --tcp=8080:8080 --dry-run=client -o yaml > test-svc.yaml` = Permet de générer un fichier de configuration (Retrouvez les autres composants en suivant `kubectl <composant> --help`| Pour les pods `kubectl run --help `)

`journalctl -u kubelet` = Affiche les logs du srevices 

`k get pod -o jsonpath="{.items[*].metadata.name}"` = Affiche les noms de tous les pods (range to list on separate line)

`k get pod -o jsonpath="{.items[*].spec.containers[0].image}"` = Affiche les images des pods (range to list on separate line)

`jsonpath and custom column`


`kubectl rollout restart deployment/mydb` = Restart tous les pods du deployment mydb. Très utile après une modif du configmap/secret monté sur les pods 
