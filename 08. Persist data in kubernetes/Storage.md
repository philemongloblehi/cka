Storage
---

### Persistent Volume
Composant Kubernetes qui définit les ressources du cluster.

Ils ne sont pas "namespaced"  

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: test-pv
spec: 
  hostPath:
    path: "/mnt/data"
  capacity: 
    storage: 10Gi
  accessModes:
  - ReadWriteMany
```
### Persistent Volume Claim
Il permet d'effectuer une demande de ressources.

Il doit être crée dans le namespace du pod réclamant

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: test-pvc
spec: 
  resources:
    requests:
      storage: 5Gi
  accessModes:
  - ReadWriteMany
```
### Storage Class
Il permet de créer des PV de facon dynamique quand les PVC les réclament
### HostPath
Il s'agit d'un type de Persistent Volume essentiellement celui demandé pour la CKA.

Il permet de monter un répertoire du noeud à l'intérieur du conteneur 

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql-deployment
  labels:
    app: mysql
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mysql
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
      - name: mysql
        image: mysql:8.0
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: mypwd
        volumeMounts:
        - name: test-storage
          mountPath: "/var/lib/mysql"
      volumes:
      - name: test-storage
        persistentVolumeClaim:
          claimName: test-pvc
```

