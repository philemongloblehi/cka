EMPTYDIR
---

Les "emptyDir" sont des stockage sans persistence 

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:

      - name: nginx
        image: nginx
        ports:
        command: ["sh","-c"]
        args:
        - while true; do
            echo $(date) Info some app data >> /var/log/myapp.log;
            sleep 5;
          done;
        ports:
          - containerPort: 80
        volumeMounts:
          - name: log-data
            mountPath: /var/log 

      - name: sidecar-busybox
        image: busybox
        volumeMounts:
          - name: log-data
            mountPath: /var/log 
        command: ["sh","-c","sleep 1000"]

      volumes:
        - name: log-data
          emptyDir: {}
```
