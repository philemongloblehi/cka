Check certificate expiration
---

`sudo kubeadm certs check-expiration`

`openssl x509 -in /etc/kubernetes/pki/ca.crt -text -noout | grep Validity -A2`
