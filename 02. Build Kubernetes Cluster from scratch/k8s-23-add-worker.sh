#!/bin/bash

# STEP 4
# Afficher la commande d'ajout 
kubeadm token create --print-join-command
# In both Worker Nodes, paste the kubeadm join command to join the cluster. Use sudo to run it as root:
sudo kubeadm join ...
# In the Control Plane Node, view cluster status (Note: You may have to wait a few moments to allow all nodes to become ready):
kubectl get nodes
