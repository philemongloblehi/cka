## Connect to a Cluster

### Use the flag 
       sudo kubectl get node --kubeconfig <config_file>
       sudo kubectl get node --kubeconfig /etc/kubernetes/admin.conf

### Use environment variable (Only in the Tab)
        sudo -i 
        export KUBECONFIG=/etc/kubernetes/admin.conf
        kubectl get node

### Make it persistent
        mkdir ~/.kube 
        sudo cp /etc/kubernetes/admin.conf ~/.kube/config
        chown user:user ~/.kube/config
        kubectl get node
