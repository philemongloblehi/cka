### Guide Mise en place d'un cluster kubernetes from scratch ?

Liens importants : https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports

- Provisionner les instances Master et Worker

- Désactiver le swapp (sudo swapoff -a)

- Configurer /etc/hosts (ip   name)

- Ouvrir les ports relativement à la documentation

- Effectuer les prérequis relativement à la documentation

- Installer un container runtime sur chaque Noeud (Si containerd, install-containerd.sh)

- Installer Kubeadm, Kubelet, Kubectl (install-k8s-component.sh)

        (List available version, apt-cache madison kubeadm)

        Kubeadm : Initialise le cluster. Il transforme une instance en control plane 

        Kubelet : Permet de créer les ressources Kubernetes 

        Kubectl : Ligne de commande pour communiquer avec le cluster 

- Initialiser le noeud master 

        sudo kubeadm init

- Installer un Container Network Interface 

        https://kubernetes.io/docs/concepts/cluster-administration/addons/

        💡 Pour overwrite le range par défaut avec weaveworks, ajouté en command --ipalloc-range=<ip/cidr>
        Le range d'ip des pods et le range d'ip des nodes ne doivent pas se chevaucher

        💡 Se référer à la documentation concernant les ports à ouvrir pour weaveworks 

- Joindre les workers aux clusters 

`kubeadm join 172.31.35.222:6443 --token 71asnl.fr29is1c19j9v1vx --discovery-token-ca-cert-hash sha256:8414c943461675d7cb2618cd22be98e4ea3818c5962b4022e206620b820bba6a`

💡 `kubeadm --help` for all options

    



