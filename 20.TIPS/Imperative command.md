Imperative 
---

`kubectl run mypod --image=nginx` = Créer un pod 

`kubectl create deployment nginx-deployment --image=nginx` = Créer un déploiement avec image nginx

`kubectl create deployment nginx-deployment --image=nginx --replicas=2` = Créer un déploiement avec image nginx et deux replicas

`kubectl expose deployment nginx-deployment --type=NodePort --name=nginx-service` =  Créer un service pour le déploiement

`kubectl create --help ` = Infos à propos des options de la commande 

`kubectl create service clusterip myservice --tcp=80:80 --dry-run=client -o yaml > myservice.yaml` =  Créer un service de type clusterIp nommé myservice écoutant sur le port 80 avec des conteneurs ecoutants sur le 80

`alias kube=kubectl`

export do="--dry-run=client -o yaml"

`kubectl create service clusterip myservice --tcp=80:80 $do > myservice.yaml`

`kubectl scale --replicas=3 deployments/mysql` = Scale to 3 replicas 
