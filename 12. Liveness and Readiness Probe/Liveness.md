Liveness Probe
---

Liveness probe allows to check the status of containers in pods 
(While application is running)

### Exec Probes

Kubelet execute the specific command to check the health of the application 

```yaml
spec:
    containers:
    - name: nginx
      image: nginx
      livenessProbe:
      periodSeconds: 5
      exec:
        command: ["cat","/tmp/healthy"]   
```

### HTTP request

Kubelet sends a HTTP request to the path and port

```yaml
spec:
    containers:
    - name: nginx
      image: nginx
      livenessProbe:
      periodSeconds: 5
      httpGet:
          path: /healthz
          port: 8080   
```

### TCP request

Kubelet try to join thhe port where the application is running (At the node level)

```yaml
spec:
    containers:
    - name: nginx
      image: nginx
      periodSeconds: 20
      livenessProbe:
        tcpSocket:
          port: 8080  
```
