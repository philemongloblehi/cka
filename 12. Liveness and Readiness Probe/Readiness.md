Readiness
---

Readiness probe ensures that the container is in a state to receive requests
(During application startup)

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: app-nginx
  labels:
    app: app-nginx
spec:
  containers:
  - name: app-nginx
    image: nginx
    ports:
    - containerPort: 8080
    readinessProbe:
      tcpSocket:
        port: 8080
      initialDelaySeconds: 10
      periodSeconds: 10
    livenessProbe:
      tcpSocket:
        port: 8080
      initialDelaySeconds: 15
      periodSeconds: 20
```
