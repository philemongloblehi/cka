```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp-resources
  labels:
    app: myapp
spec:
  replicas: 1
  selector:
    matchLabels:
      app: myapp
  template:
    metadata:
      labels:
        app: myapp
    spec:
      containers:

      - name: myapp
        image: nginx
        resources:
          requests:
            memory: "64Mi"
            cpu: "250m"
          limits:
            memory: "128Mi"
            cpu: "500m"
 
      - name: sidecar-busybox
        image: busybox
        command: ["sh","-c","sleep 1000"]
        resources:
          requests:
            memory: "128Mi"
            cpu: "100m"
          limits:
            memory: "256Mi"
            cpu: "500m"

```
