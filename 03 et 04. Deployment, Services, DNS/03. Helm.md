### Helm 

Helm est un package manager pour Kubernetes

On peut héberger et retrouvez des helm charts sur le Helm Hub

Exemple : Helm chart pour installer nginx-controller

`helm repo add bitnami https://charts.bitnami.com/bitnami`

`helm repo update`

`helm install k8s-ingress-controller bitnami/nginx-ingress-controller`

`helm list`
